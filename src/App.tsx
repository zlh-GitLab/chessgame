import React, {
  // useState,
  // useCallback
} from 'react';
// import logo from './logo.svg';
import './App.css';
// import CountComp from './components/CountComp';
// import ChessComp from './components/ChessComp';
// import BoardComp from './components/BoardComp';
import GameComp from './components/GameComp';
// import { ChessType } from './types/enums';

// const types: ChessType[] = [
//   ChessType.black,
//   ChessType.black,
//   ChessType.red,
//   ChessType.black,
//   ChessType.black,
//   ChessType.red,
//   ChessType.none,
//   ChessType.black,
//   ChessType.none,
// ]

function App() {
  // const [num, setNum] = useState(0);

  // const onClick = useCallback((index) => {
  //   console.log(index);
  // }, [])

  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}


      {/* <CountComp num={num} onChange={n => setNum(num + n)} /> */}


      {/* <ChessComp onClick={e => console.log('被点击了', e)} type={ChessType.red} />
      <ChessComp onClick={e => console.log('被点击了', e)} type={ChessType.black} />
      <ChessComp onClick={e => console.log('被点击了', e)} type={ChessType.none} /> */}

      {/* <BoardComp chesses={types} onClick={onClick} /> */}

      <GameComp />

    </div>
  );
}

export default App;
