// 枚举会出现在编译结果中，编译结果中表现为对象。
export enum ChessType {
  // 棋子类型枚举
  none, // 0
  red, // 1
  black // 2
}

export enum NextChess {
  red = ChessType.red,
  black = ChessType.black
}

export enum GameStatus {
  // 游戏状态
  gaming, // 游戏中
  redWin, // 红方胜利
  blackWin, // 黑方胜利
  equal, // 平局
}