import React, { useEffect, useState } from 'react';
import { ChessType } from '../../types/enums';
import classNames from 'classnames';
import './index.css';

interface IProps {
  type: ChessType,
  onClick?: (type: ChessType) => void,
}

const getChessClassName: (type: ChessType) => string = (type) => {
  switch(type) {
    case ChessType.black:
      return 'black';
    case ChessType.red:
      return 'red';
    default:
      return '';
  }
}

export default function ChessComp(props: IProps) {
  const { type, onClick } = props;


  const [chessClassName, setChessClassName] = useState(getChessClassName(type))

  useEffect(() => {
    setChessClassName(getChessClassName(type));
  }, [type]);

  const handleClick = () => {
    type === ChessType.none && onClick && onClick(type);
  }

  return (
    <div className="chess" onClick={handleClick}>
      <div className={classNames(chessClassName, 'chess-item')}></div>
    </div>
  )
}