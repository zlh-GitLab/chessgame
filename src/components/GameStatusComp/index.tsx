import { GameStatus, ChessType } from "../../types/enums"
import './index.css';

interface IProps {
  status: GameStatus,
  next: ChessType.red | ChessType.black,
}

export default function GameStatusComp(props: IProps) {
  let content: JSX.Element;
  switch(props.status) {
    case GameStatus.gaming:
      content = (props.next === ChessType.red
        ? <div className="next red">红方落子</div>
        : <div className="next black">黑方落子</div>);
      break;
    case GameStatus.redWin:
      content = <div className="win red">红方胜利</div>;
      break;
    case GameStatus.blackWin:
      content = <div className="win black">黑方胜利</div>;
      break;
    default:
      content = <div className="equal">平局</div>
  }
  return (
    <div className="game-status">{content}</div>
  )
}