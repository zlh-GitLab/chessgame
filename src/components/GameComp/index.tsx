import React from 'react';
import { ChessType, GameStatus } from '../../types/enums';
import BoardComp from '../BoardComp';
import GameStatusComp from '../GameStatusComp';

interface IState {
  chesses: ChessType[] // 棋盘上的全部棋子
  gameStatus: GameStatus // 游戏状态
  nextChess: ChessType.red | ChessType.black // 下一个棋子的颜色
  // nextChess: NextChess
}

/**
 * 判断给定的数组，里面的元素是不是全都相等
 * @param arr 
 */
function checkWin(arr: ChessType[]): boolean {
  const setTemp = new Set(arr);
  // 如果去重后只有一个元素，并且该元素不是ChessType.none则表明有一方胜利了
  return setTemp.size === 1 && !setTemp.has(ChessType.none);
}

export default class GameComp extends React.Component<{}, IState> {

  state:IState = { // 这里必须要再次约束
    chesses: [],
    gameStatus: GameStatus.gaming,
    nextChess: ChessType.red,
    // name: 'zhanglnhao'
  }

  /**
   * 初始化数据
   */
  init() {
    this.setState({
      chesses: Array(9).fill(true).map(() => ChessType.none),
      gameStatus: GameStatus.gaming,
      nextChess: ChessType.red,
      // name: 'zlh',
    })
  }

  getGameStatus(chesses: ChessType[], index: number): GameStatus {
    // 1. 判断是否有一方获得胜利
    /**
     * 棋盘如下
     * 0 1 2
     * 3 4 5
     * 6 7 8
     * 
     * 横向
     * 1：0 - 0 + 2
     * 2：0 - 0 + 2
     * 4：3 - 3 + 2
     * 6：6 - 6 + 2
     * 8：6 - 6 + 2
     * 结论：落子为index位置，需要判断的横向范围为：
     * [Math.floor(index / 3) * 3, Math.floor(index / 3) * 3 + 2]
     * 
     * 纵向
     * 1: 1 4 7
     * 4: 1 4 7
     * 3: 0 3 6
     * 2: 2 5 8
     * 8: 2 5 8
     * 结论：落子为index位置，需要判断的纵向范围为：
     * [index % 3, index % 3 + 3, index % 3 + 6]
     * 
     * 斜线
     * 落子index为1, 3, 5, 7不需要判断，
     * 只需要判断[0, 4, 8]和[2, 4, 6]即可
     */
    const rowMin = Math.floor(index / 3) * 3;
    const rowMax = rowMin + 2;
    const rows = chesses.slice(rowMin, rowMax);

    const colMin = index % 3;
    const cols = [chesses[colMin], chesses[colMin + 3], chesses[colMin + 6]];

    const slant1 = [chesses[0], chesses[4], chesses[8]];
    const slant2 = [chesses[2], chesses[4], chesses[6]];
    if(checkWin(rows) || checkWin(cols) || checkWin(slant1) || checkWin(slant2)) {
      // 行方向有一方胜利，如果落子chesses[index]是红方，则是红方胜利，否则黑方胜利
      // alert('有一方胜利了');
      return chesses[index] === ChessType.red ? GameStatus.redWin : GameStatus.blackWin;
    }

    // 2. 判断是否平局
    if (!chesses.includes(ChessType.none)) {
      // 没有空位了即平局，怎么可以提前判断是否平局呢？
      return GameStatus.equal;
    }

    // 3. 游戏正在进行中
    return GameStatus.gaming;
  }

  /**
   * 该函数执行世纪：游戏没有结束并且点击的位置没有棋子
   * @param index 
   */
  handleChessClick = (index:number) => {
    // 更改棋盘数据
    const newChesses = [...this.state.chesses];
    newChesses[index] = this.state.nextChess;

    this.setState(prevState => ({
      chesses: newChesses,
      // 更改nextChess，比如红方过后是黑方
      nextChess: prevState.nextChess === ChessType.red ? ChessType.black : ChessType.red,
      gameStatus: this.getGameStatus(newChesses, index),
    }))
  }

  componentDidMount() {
    this.init();
  }

  render() {
    return (
      <div className="game-wrap">
        <h2>井字棋游戏</h2>
        <GameStatusComp status={this.state.gameStatus} next={this.state.nextChess} />
        <BoardComp
          chesses={this.state.chesses}
          isGameOver={this.state.gameStatus !== GameStatus.gaming}
          onClick={this.handleChessClick}
        />
        <button
          onClick={this.init.bind(this)}
          disabled={this.state.gameStatus === GameStatus.gaming}
        >
          重新开始
        </button>
      </div>
    )
  }
}