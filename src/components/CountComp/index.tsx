import React from 'react';

interface IProps{
  num: number,
  onChange?: (n:number) => void, 
}

interface IState{
  msg: string,
  description: string,
}

// export default function CountComp(props: IProps) {
//   const { num, onChange } = props;
//   const handleChange: (n: number) => void = (n) => {
//     onChange && onChange(n);
//   }
//   return (
//     <div className="count-comp">
//       <button onClick={() => handleChange(1)}>increate</button>
//       <span>{num}</span>
//       <button onClick={() => handleChange(-1)}>decrease</button>
//     </div>
//   )
// }


// const CountComp: React.FC<IProps> = (props) => {
//   const { num, onChange } = props;
//   const handleChange: (n: number) => void = (n) => {
//     onChange && onChange(n);
//   }
//   return (
//     <div className="count-comp">
//       <button onClick={() => handleChange(1)}>increate</button>
//       <span>{num}</span>
//       <button onClick={() => handleChange(-1)}>decrease</button>
//     </div>
//   )
// }

// export default CountComp;

export default class CountComp extends React.Component<IProps, IState> {

  state = {
    msg: '',
    description: '',
  }

  handleChange:(n: number) => void = (n)  => {
    this.props.onChange?.(n);
    
    // this.setState({
    //   msg: 'Test CountComp',
    // })
  }


  render() {
    return (
      <div className="count-comp">
        <button onClick={() => this.handleChange(1)}>increate</button>
        <span>{this.props.num}</span>
        <button onClick={() => this.handleChange(-1)}>decrease</button>
      </div>
    )
  }
}