import React, { useMemo } from 'react';
import ChessComp from '../ChessComp';
import { ChessType } from '../../types/enums';
import './index.css';

interface IProps {
  chesses: Array<ChessType> // ChessType[]
  onClick?: (index: number) => void
  isGameOver?: boolean
  /**
   * 怎么给isGameOver赋默认值？1：设为可选属性，然后设置defaultProps
   * 不能在接口interface直接书写默认值，因为接口不会打包到编译结果，当你这么书写的时候TypeScript也会发出警告
   */
}

// export default function BoardComp(props: IProps) {
const BoardComp: React.FC<IProps> = (props) => {
  const { chesses, onClick, isGameOver } = props;
  /**
   * 这里把鼠标放到isGameOver上，会发现TypeScript推断isGameOver为boolean | undefined，
   * 但是该组件isGameOver是有默认值的，故不会存在undefined的情况，怎么告诉TypeScript呢？如下：
   * const isGameOver = props.isGameOver as boolean;
   * 或者
   * const isGameOver = props.isGameOver!; 感叹号的意思是非空断言，告诉TS不用考虑数据为空的情况；
   */

  const chessList = useMemo(() => {
    return chesses.map((type, index) => (
      <ChessComp
        key={index}
        type={type}
        onClick={() => !isGameOver && onClick && onClick(index)}
      />
    ))
  }, [chesses, isGameOver, onClick]);
  
  return (
    <div className="board">
      {chessList}
    </div>
  )
}

BoardComp.defaultProps = {
  /**
   * 如果给函数组件赋默认值，function BoradComp(props) {}这种写法跟普通函数没有什么区别，
   * 导致TypeScript并不知道BoardComp是一个函数组件，在书写defaultProps的时候就没有了智能提示，
   * 书写IProps以外的属性也不会提示。
   * 所以要换成 const BoardComp: React.FC<IProps> = (props) => {}这种写法，届时书写IProps以外的
   * 属性时TypeScript就会提示你。
   */
  isGameOver: false,
}

export default BoardComp;